if (!process.env.FLICKR_API_KEY) {
  throw `Flickr api key is required. `
}

const FLICKR_API_KEY = process.env.FLICKR_API_KEY
const FLICKR_PHOTO_ID = 42167364992

import { assert } from 'chai'
import HexoTagFlickrResponsive from '../src/hexo_tag_flickr_responsive'

describe('hexo-tag-flickr-responsive', () => {
  let args = [FLICKR_PHOTO_ID]
  let hexo = {
    config: {
      flickr_responsive: {
        api_key: FLICKR_API_KEY
        //cache_file_path: '.unko.json',
        //cache_ttl: 86400000
      }
    }
  }

  // @todo http://www.chaijs.com/plugins/chai-as-promised/
  it('Normal', () => {
    new HexoTagFlickrResponsive(hexo)
      .process(args)
      .then(tag => {
        assert.isOk()
      })
      .catch(err => {
        assert.isNotOk()
      })
  })

  it(
    'Custom renderer' /*, () => {
    let _hexo = hexo
    _hexo.config.flickr_responsive.renderer = "../lib/render/examples/href_to_flickr"
    new HexoTagFlickrResponsive(_hexo)
      .process(args)
      .then(tag => {
        console.log(tag)
        assert.isOk()
      })
      .catch(err => {
        assert.isNotOk()
      })
  }*/
  )

  // WIP below
  it('No photo')
  it('Inalid API Key')
  it('Custom template')
  it('Flickr API caching')
})
