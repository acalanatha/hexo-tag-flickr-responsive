A hexo plugin for generation responsive `<img>` tag using Flickr API response.

Generate img tag as below.

```
<img src="https://farm1.staticflickr.com/973/42167364992_717cced469.jpg"
  srcset="https://farm1.staticflickr.com/973/42167364992_717cced469_t.jpg 100w,
          https://farm1.staticflickr.com/973/42167364992_717cced469_m.jpg 240w,
          https://farm1.staticflickr.com/973/42167364992_717cced469_n.jpg 320w,
          https://farm1.staticflickr.com/973/42167364992_717cced469.jpg 500w,
          https://farm1.staticflickr.com/973/42167364992_717cced469_z.jpg 640w,
          https://farm1.staticflickr.com/973/42167364992_717cced469_c.jpg 800w,
          https://farm1.staticflickr.com/973/42167364992_717cced469_b.jpg 1024w,
          https://farm1.staticflickr.com/973/42167364992_c6e36905ad_h.jpg 1600w,
          https://farm1.staticflickr.com/973/42167364992_47a49a5b5c_k.jpg 2048w"
  alt="大日本プロレス後楽園ホール大会"
>
```

## Installation & Configuration
```
npm install hexo-tag-flickr-responsive --save
```
```
flickr_responsive:
  api_key: fc492c16b3abacfdbad8481e2384e142
```

## Syntax
```
{% flickr_responsive photo_id %}
```

## How to generate your customized tags
Default rendering tag function is replaceable.

At first, Write your tag building function and save it under hexo root path. [sample](https://bitbucket.org/acalanatha/hexo-tag-flickr-responsive/src/fdf6c02d09e58caa5d126dc5cb20aefe17bac523/lib/render/examples/simple.js)
```
module.exports = function(ctx) {
    // Let's write code to build tags using ctx object that  included with API Response and utility method.
    return "<your excellent tag/>"
}
```

Next, add your function locale to _config.yml
```
flickr_responsive:
  api_key: fc492c16b3abacfdbad8481e2384e142
+  renderer: "path/to/your/function.js"
```

## Flickr API caching
```
flickr_responsive:
  api_key: ff494c56b3dbae3dbdd7480e5384e262
+  cache_file_path: path/to/角刈りにしてこい.json
+  cache_ttl: 86400000
```