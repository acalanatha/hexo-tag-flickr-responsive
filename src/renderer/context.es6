import Enumerable from 'linq'

const FALLBACK_IMG_WIDTH_MAX = 500
const EXCLUDE_IMG_LABEL_REGEX = /(Square|Original)/

export default class RenderContext {
  constructor(imageInfo, imageSizes, pluginArgs) {
    this.imageInfo = imageInfo
    this.imageSizes = imageSizes
    this.pluginArgs = pluginArgs
  }

  getTitle() {
    return this.imageInfo.title._content
  }

  getDescription() {
    return this.imageInfo.description._content
  }

  getContentUrl(type = 'photopage') {
    return Enumerable.from(this.imageInfo.urls.url)
      .where(x => x.type == type)
      .elementAtOrDefault(0, {})._content
  }

  getFallbackImageDetail(labelOrWidth = FALLBACK_IMG_WIDTH_MAX, property) {
    let detail = Enumerable.from(this.imageSizes.size)
      .orderByDescending(x => Number.parseInt(x.width))
      .where(x => (typeof labelOrWidth == 'string' ? x.label == labelOrWidth : x.width <= labelOrWidth))
      .elementAtOrDefault(0, {})

    return property ? detail[property] : detail
  }

  getSrcsetValue(delim = `,\n`, excludeLabelName = EXCLUDE_IMG_LABEL_REGEX) {
    return Enumerable.from(this.imageSizes.size)
      .orderBy(x => Number.parseInt(x.width))
      .where(x => !x.label.match(excludeLabelName))
      .toArray()
      .map(x => {
        return `${x.source} ${x.width}w`
      })
      .join(delim)
  }
}
