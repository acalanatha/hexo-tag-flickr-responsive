import ejs from 'ejs'

module.exports = ctx => {
  let tpl = `<img src="<%= src %>"
  srcset="<%= srcset %>"
  alt="<%= title %>"
  >`

  let datum = {
    src: ctx.getFallbackImageDetail('source'),
    title: ctx.getTitle(),
    srcset: ctx.getSrcsetValue(`,\n` + ' '.repeat('  srcset="'.length))
  }

  return ejs.render(tpl, datum)
}
