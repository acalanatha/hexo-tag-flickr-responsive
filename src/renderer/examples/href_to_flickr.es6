const ejs = require('ejs')

module.exports = ctx => {
  let tpl = `<a href="<%= href %>">
  <img src="<%= fallback.source %>"
       srcset="<%= srcset %>"
       alt="<%= title %>"
       class="my class">
</a>`

  let datum = {
    fallback: ctx.getFallbackImageDetail(),
    title: ctx.getTitle(),
    srcset: ctx.getSrcsetValue(),
    href: ctx.getContentUrl()
  }

  return ejs.render(tpl, datum)
}
