const ejs = require('ejs')

module.exports = ctx => {
  // Let's write code to build tags using ctx object that  included with API Response and utility method.

  let tpl = `<img src="<%= fallback.source %>"
  srcset="<%= srcset %>"
  alt="<%= title %>"
  class="my class"
  >`

  let datum = {
    fallback: ctx.getFallbackImageDetail(),
    title: ctx.getTitle(),
    srcset: ctx.getSrcsetValue()
  }

  return ejs.render(tpl, datum)
}
