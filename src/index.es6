import HexoTagFlickrResponsive from './hexo_tag_flickr_responsive'

const PLUGIN_NAME = 'flickr_responsive'

hexo.extend.tag.register(
  PLUGIN_NAME,
  args => {
    return new HexoTagFlickrResponsive(hexo)
      .process(args)
      .then(tag => {
        return tag
      })
      .catch(err => {
        hexo.log.error(err)
        throw err
      })
  },
  {
    async: true,
    end: false
  }
)
