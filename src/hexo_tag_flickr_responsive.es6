import FlickrSDKCacheWrapper from './flickr_sdk_cache_wrapper'
import RenderContext from './renderer/context'
import path from 'path'

const DEFAULT_CONFIG = {
  renderer: './renderer/default'
}

export default class HexoTagFlickrResponsive {
  constructor(hexo) {
    this.hexo = hexo
    this.config = Object.assign(DEFAULT_CONFIG, hexo.config.flickr_responsive || {})

    if (!this.config.api_key) {
      throw '(__config.yml).flickr_responsive.api_key is required.'
    }
    this.flickr = new FlickrSDKCacheWrapper(
      this.config.api_key,
      this.config.cache_file_path || false,
      this.config.cache_ttl || false
    )
  }

  process(args) {
    let photoId = args[0]
    let queue = [this.flickr.photos.getInfo({ photo_id: photoId }), this.flickr.photos.getSizes({ photo_id: photoId })]

    return Promise.all(queue)
      .then(results => {
        let info = results[0].body.photo
        let sizes = results[1].body.sizes
        return this.__buildTag(info, sizes, args)
      })
      .catch(err => {
        throw err
      })
  }

  __buildTag(info, sizes, args) {
    let renderer = null
    let pathsToRender = [
      this.config.renderer,
      path.join(__dirname, this.config.renderer),
      path.join(process.cwd(), this.config.renderer),
      path.join('../../../', this.config.renderer)
    ]
    pathsToRender.some(expectedPath => {
      try {
        renderer = require(expectedPath)
        return true
      } catch (e) {
        renderer = e
      }
    })

    if (typeof renderer == 'function') {
      return renderer.bind(this)(new RenderContext(info, sizes, args))
    } else {
      throw renderer
    }
  }
}
