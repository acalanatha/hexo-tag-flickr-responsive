import Flickr from 'flickr-sdk'
import InCache from 'incache'
import md5 from 'md5'

/**
 * @classdesc flickr-sdkと同じI/Fで使えるwrapper, flickr-sdkの実行結果をインターセプトしてキャッシュする
 */
export default class FlickrSDKCacheWrapper {
  constructor(apiKey, cacheFilePath, cacheTTL) {
    this.sdk = new Flickr(apiKey)
    this.cacheTTL = cacheTTL
    this.cacheStorage =
      cacheFilePath && cacheTTL ? new InCache({ autoSave: true, filePath: cacheFilePath }) : new InCache()

    // @see https://stackoverflow.com/questions/37714787/can-i-extend-proxy-with-an-es2015-class
    // @todo fix proxy hell
    return new Proxy(this.sdk, {
      get: (target, prop) => {
        if (!target instanceof Flickr) {
          return target[prop]
        }
        let namespace = prop
        return new Proxy(
          {},
          {
            get: (target, method) => {
              return function() {
                return this.__callSdkMethod(namespace, method, arguments)
              }.bind(this)
            }
          }
        )
      }
    })
  }

  __callSdkMethod(namespace, method, args) {
    let token = md5(JSON.stringify(arguments))
    let cachedResp = this.cacheStorage.get(token)

    if (cachedResp) {
      return new Promise(() => {
        return cachedResp
      })
    }

    return this.sdk[namespace][method](args[0])
      .then(
        function(resp) {
          this.cacheStorage.set(token, resp, this.cacheTTL ? { maxAge: this.cacheTTL } : {})
          return resp
        }.bind(this)
      )
      .catch(err => {
        throw err
      })
  }
}
